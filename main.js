async function findYouIP() {
    try {
        const response = await fetch('https://api.ipify.org/?format=json');
        const data = await response.json();
        const clientIP = data.ip;

        const apiResponse = await fetch
        (`http://ip-api.com/json/${clientIP}?fields=status,message,continent,continentCode,country,countryCode,region,regionName,city,district,zip,lat,lon,timezone,offset,currency,isp,org,as,query`);
        const apiData = await apiResponse.json();

        const resultDiv = document.getElementById('resultIp');
        resultDiv.innerHTML = `
                    <p>Continent: ${apiData.continent}</p>
                    <p>Country: ${apiData.country}</p>
                    <p>Region: ${apiData.regionName}</p>
                    <p>City: ${apiData.city}</p>
                    <p>Disctric: ${apiData.district}</p>
                `;
    } catch (err) {
        console.error(err);
    }
}